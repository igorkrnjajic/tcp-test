#!/usr/bin/env python3
import socket
import threading
import argparse

class TcpTest():

    def __init__(self, host, ports, num_of_threads=10, timeout=2):
        self.host = host
        self.ports = ports
        self.num_of_threads = num_of_threads
        self.results = []
        self.threads = []
        socket.setdefaulttimeout(timeout)

    def tcp_test(self, port):
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            s.connect((self.host, port))
            result = 'Connection successful'

        except socket.timeout:
            result = 'Timed out'

        except Exception as e:
            result = e.strerror

        finally:
            s.close()
            self.results.append({
                'host': self.host,
                'port': port,
                'result': result
            })
            print("{:<20}{:<10}{:<20}".format(self.host, port, result))

    def worker(self):
        if self.ports:
            port = self.ports.pop(0)
            t = threading.Thread(target=self.tcp_test,args=(port,))
            t.start()
            self.threads.append(t)
    
    def run(self):
        while self.ports:
            for _ in range(self.num_of_threads):
                self.worker()

            for t in self.threads:
                t.join()

def main():
    parser = argparse.ArgumentParser('tcp-test', description='Test connectivity to remote host via TCP ports')
    parser.add_argument('host', metavar='HOSTNAME/IP', help='Remote host to test')
    parser.add_argument('--ports', nargs='*', type=int, help='List of comma-separated port numbers to test. If omitted, well known ports will be used.')
    parser.add_argument('--num-of-threads', type=int, default=1, help='Number of concurrent threads to execute. Could speed things up if testing a large number of ports. Default is one thread.')
    parser.add_argument('--timeout', type=int, default=2, help='How many seconds to wait before marking a connection as "Timed out". Default is 2 seconds.')
    args = vars(parser.parse_args())

    well_known_ports = [
        20, #FTP (data transfer)
        21, #FTP (control)
        22, #SSH
        23, #Telnet
        25, #SMTP
        43, #WHOIS
        49, #TACACS
        53, #DNS
        80, #HTTP
        88, #Kerberos
        109, #POP2
        110, #POP3
        111, #ONC RPC
        115, #SFTP
        118, #SQL
        135, #RPC
        137, #NETBIOS Name Service
        139, #NETBIOS Session Service
        143, #IMAP
        152, #BFTP
        153, #SGMP
        156, #SQL
        162, #SNMP Trap
        170, #PostScript print server
        179, #BGP
        201, #AppleTalk
        220, #IMAP3
        264, #BGMP
        311, #macOS Server Admin
        387, #AppleTalk
        389, #LDAP
        443, #HTTPS
        445, #SMB
        464, #Kerberos
        465, #SMTPS
        500, #ISAKMP
        530, #RPC
        543, #Kerberos
        544, #Kerberos
        546, #DHCPv6 Client
        547, #DHCPv6 Server
        587, #Email submission
        593, #MS Exchange
        601, #Reliable Syslog
        636, #LDAPS
        647, #DHCP Failover
        660, #macOS Server Administration
        691, #MS Exchange
        694, #Linux HA Heartbeat
        749, #Kerberos
        829, #Certificate Management
        847, #DHCP Failover
        853, #DNS over TLS
        860, #iSCSI
        861, #OWAMP
        862, #TWAMP
        873, #rsync
        953, #BIND
        989, #FTPS
        990, #FTPS
        992, #Telnet over TLS
        993, #IMAPS
        995, #POP3S
        1080, #SOCKS proxy
        1085, #WebObjects (java)
        1098, #RMI (java)
        1099, #RMI (java)
        1194, #OpenVPN
        1220, #QuickTime Streaming Server
        1270, #MS SCOM
        1293, #IPSec
        1360, #Mimer SQL
        1433, #MSSQL
        1434, #MSSQL Monitor
        1512, #MS WINS
        1589, #Cisco VQP
        1701, #L2F / L2TP
        1723, #PPTP
        1755, #MS Media Services
        1801, #MS Message Queuing
        1812, #RADIUS
        1813, #RADIUS
        1985, #HSRP
        1998, #Cisco XOT
        2000, #Cisco SCCP
        2049, #NFS
        2083, #Secure RADIUS
        2375, #Docker API
        2376, #Docker API (SSL)
        2377, #Docker Swarm
        2399, #ODBC/JDBC
        2424, #OrientDB
        2480, #OrientDB
        2483, #Oracle DB
        2484, #Oracle DB (SSL)
        2638, #SQL Anywhere
        2967, #Symantec System Center
        3020, #CIFS
        3225, #FCIP
        3260, #iSCSI
        3268, #MS Global Catalog
        3269, #MS Global Catalog (SSL)
        3283, #Apple Remote Desktop
        3305, #Odette FTP
        3306, #MySQL
        3389, #MS RDP
        3527, #MS Message Queuing
        3544, #Teredo tunneling
        3702, #WS-Discovery
        4172, #Teradici PCoIP
        5432, #PostgreSQL
        5480, #VMware Virtual Appliance
        5671, #AMQP (SSL)
        5672, #AMQP
        5722, #MS DFSR
        5900, #VNC
        5938, #TeamViewer
        6514, #SYSLOG over TLS
        6600, #MS Hyper-V
        6601, #MS Forefront Identity Threat Management
        6619, #Odette FTPS
        6622, #Multicast FTP
        6653, #OpenFlow
        8000,
        8080,
        8149, #Puppet
        8443,
        9443
    ]

    host = args['host']
    ports = args['ports'] if args['ports'] else well_known_ports
    num_of_threads = args['num_of_threads']
    timeout = args['timeout']

    print("{:<20}{:<10}{:<20}".format('Host/IP', 'Port', 'Result'))
    print("{:-<51}".format(''))

    t = TcpTest(host,ports,num_of_threads,timeout)
    t.run()

if __name__ == '__main__':
    main()
